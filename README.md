# Example of ci status pages

## FELIX

- production: https://felix-ci-status.app.cern.ch
- alpha: https://felix-ci-status-alpha.app.cern.ch

## OTP

- production: https://otp-ci-status.app.cern.ch
- alpha: https://otp-ci-status-alpha.app.cern.ch

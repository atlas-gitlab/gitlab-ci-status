<!DOCTYPE html>

    <?php
    include("config.php");
    $branches = explode(",", $branches);
    ?>

    <html>
        <head>
            <meta http-equiv="Content-type" content="text/html; charset=utf-8">
            <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no">
            <title><?php print(strtoupper($project)); ?> - CI - Status</title>

            <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.6.0/css/bootstrap.css">
            <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.13.6/css/dataTables.bootstrap4.min.css">

            <script type="text/javascript" language="javascript" src="https://code.jquery.com/jquery-3.7.0.js"></input>
            <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.13.6/js/jquery.dataTables.min.js"></script>

            <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.13.6/js/jquery.dataTables.min.js"></script>
            <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.13.6/js/dataTables.bootstrap4.min.js"></script>

            <script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.4/moment.min.js"></script>

            <script type="text/javascript" language="javascript" src="gitlab-ci-status.js"></script>
        </head>
        <body>
            <table id="status" class="table table-striped table-bordered nowrap" style="width:100%">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Module Name</th>

                        <?php
                        foreach ($branches as $branch) {
                            echo "<th>Branch $branch</th>";
                        }
                        ?>

                        <th>Most Recent 5 Other Branches/Tags</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th></th>
                        <th>Updates every 30s</th>
                        <th>Version: <?php include 'version.txt' ?></th>

                        <?php
                        foreach ($branches as $branch) {
                            echo "<th></th>";
                        }
                        ?>

                    </tr>
                </tfoot>
            </table>
        </body>
    </html>

$(document).ready(function() {

    //function ajaxInterval() {
    var table = $('#status').DataTable( {
        "ajax": {
            url: "gitlab-ci-status-data.php?load=0"
        },
        "paging": false,
        "searching": false,
        "autoWidth": true,
        "info": false,         
    } );   

    setInterval( function () {
        table.ajax.url("gitlab-ci-status-data.php?load=1").load();
        table.ajax.reload( null, false ); // user paging is not reset on reload
    }, 30*1000 );//secondes
} );

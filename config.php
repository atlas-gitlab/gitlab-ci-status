<?php

error_reporting(E_ALL & ~E_STRICT);
ini_set('display_errors', '1');

$token = null;
$dev_token = null;
$project = null;

$local_config = 'config-local.php';
$path = stream_resolve_include_path($local_config);
if ($path && file_exists($path)) {
   include($local_config);
}

$token = getenv("TOKEN") ?: $token;
if (!$token) {
   header("HTTP/1.0 502 Bad Gateway");
   die("TOKEN not defined\n");
}

$dev_token = getenv("DEV_TOKEN") ?: $dev_token;

$project = getenv("PROJECT") ?: $project;
if (!$project) {
   header("HTTP/1.0 502 Bad Gateway");
   die("PROJECT not defined\n");
}


$per_page = 100;                         // The number of projects per page on the webpage
include($project.".php");

?>

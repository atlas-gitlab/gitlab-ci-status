<?php
//CONFIGURATION


include('config.php');

$branches = explode(",", $branches);
$projects = explode(",", $projects);

//DETERMINE LOAD TIME
$long_load = $_GET['load']; // 0 = false, 1 = true

//FUNCTIONS
function since($datetime, $level=6) {
    /* This function time from the last update.
    it determines the amount of time between them in the most relevant way (years, month, days,...)
    level is set to 1 later to only get the highest amount of time
    it returns the data as a string*/
    $date = new DateTime($datetime);                    //Time of last update
    $date = $date->diff(new DateTime());                //Today-last_update
    $since = array_combine(array('Y', 'M', 'D', 'h', 'min', 'sec'), explode(',', $date->format('%y,%m,%d,%h,%i,%s')));  // Format to array
    $since = array_filter($since);                      //Remove all values where the time is zero
    $since = array_slice($since, 0, $level);            //Only the first $level date values

    $last_key = key(array_slice($since, -1, 1, true));  //get the index value of the last value in $since (e.g. in case you want days, months and seconds)
    $string = '';                                       //Empty string to put data in
    foreach ($since as $key => $val) {                  //For each value in since
        // separator
        if ($string) {                                  //Don,t do this the first loop since string is empty
            $string .= $key != $last_key ? ', ' : ' and '; //  concatenate " and " if this is not the last value
        }
        // add date value
        $string .= $val . ' ' . $key;                   //concatenate the value and key of since (e.g. 3 year, 5 month)
    }
    return $string;                                     //return the complete string with the data
}

function get($url, $token) {
    /* This function opens an url.
    A token is supplied to get data from gitlab
    It returns the data from that url as a string*/
    $ch = curl_init();                                  //Initiate the url
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);    //stop cURL from verifying the peer's certificate
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);     //return the transfer as a string of the return value of curl_exec()
    curl_setopt($ch, CURLOPT_HTTPHEADER,array("PRIVATE-TOKEN: $token"));    //requests both to servers and proxies with the given token
    curl_setopt($ch, CURLOPT_URL,$url);                 //Input the url to the function
    $result=curl_exec($ch);                             //execute curl and saves the data
    curl_close($ch);                                    //Close the url
    return json_decode($result, true);                  //Return the decoded . This is the pipelines
}

function startswith($haystack, $needle) {
    // Check if the string $haystack starts with the substring $needle
    $length = strlen($needle);      //Length of the substring
    return (substr($haystack, 0, $length) === $needle); //Checks if the first $length elements of $haystack are $needle. returns true/false
}

function find_first($pipelines, $branch) {
    // Find the branches specified in the beginning
    if ($pipelines) {                           //If $pipelines is not empty
        foreach($pipelines as $pipeline) {      //For each $pipeline in $pipelines
            if ($pipeline['ref'] == $branch) {  //If the reference (ref) of the pipeline is the branch you are looking for
                return $pipeline;               //reurn it
            }
        }
    }
    return array(); //return empty array
}

function find_other_branches($pipelines, $branches, $max_branches) {
    // Find the $max_branches number of other pipelines that are not in the specified $branches
    $other_branches = array();              //Empty array to save the branches
    $count = 0;                             //Count up to $max_branches
    if ($pipelines) {                       //If the array is not empty
        foreach($pipelines as $pipeline) {  //For each $pipeline in $pipelines
            $branch = $pipeline['ref'];     //Branch = the reference of the pipeline
            if (!in_array($branch, $branches)) {    //If the branch is not in branches
                if (!in_array($branch, $other_branches)) {  //If the branch in not already in $other_branches
                    $other_branches[] = $branch;            //append $branch to $other_branches
                    $count++;                               //$count increase
                    if ($count >= $max_branches) {          //If the $count reaches $max_branches
                        break;                              //stop the loop
                    }
                }
            }
        }
    }
    return $other_branches;                 //return the found branches
}

function find_job($group, $project, $pipeline, $token) {
    // find relevant the relevant job from all the job in the pipeline and return it
    if (array_key_exists('id', $pipeline)) {                    //if the key 'key' exists in $pipelines
        $pipeline_id = $pipeline['id'];                         // define $pipeline_id
        if ($pipeline_id == "") {                               //if pipeline['id'] is empty
            return [array(),array()];                                     //return an empty array
        }

        $url = "https://gitlab.cern.ch/api/v4/projects/$group%2F$project/pipelines/$pipeline_id/jobs";  //Create an url with the variables: $group,$project & $pipeline_id
        $jobs = get($url, $token);                                                                      // Get the data from the url above

        $failed_job = array();
        foreach($jobs as $job) {
            if ($job["status"] != "success") {
                $failed_job = $job;
            }
        }

        // first try to find dedicated job
        foreach($jobs as $job) {
            if (startswith($job['name'], 'config-build-test')) {
                return [$job,$failed_job];
            }
        }

        // now try others       return jobs that start with "test-","build:" & "build-"
        foreach($jobs as $job) {
            switch (startswith($job['name'], 'config-build-test')) {      //Switch through different statuses of the branch and define a corresponding $btn_class
                case "test-":
                    return [$job,$failed_job];
                case "build:":
                    return [$job,$failed_job];
                case "build-":
                    return [$job,$failed_job];
            }
        }
    }

    return [array(),array()];
}

function label($group, $project) {
    // create the first row of the website to the project in that row
    $project_url = "https://gitlab.cern.ch/$group/$project/-/pipelines/";   //url to navigate to
    return "<a class=\"btn btn-outline-primary\" href=\"$project_url\" role=\"button\" target=\"_$project\">$project</a>";  //create a button with he url
}

function button($project, $pipeline, $url, $text) {
    /* create a  button for a branch with a branch name on it
    each button has a different color depending on the status of the branch
    clicking the button will lead to the branches that in on the button*/
    if (!array_key_exists('ref', $pipeline)) {  //exists if there is not a reference in the pipline
        return "";
    }
    $branch = $pipeline['ref'];     //define the branch
    if ($branch == "") {            //and exit if it is empty
        return "";
    }

    switch ($pipeline['status']) {      //Switch through different statuses of the branch and define a corresponding $btn_class
        case "success":
            $btn_class = "btn-success";
            break;
        case "failed":
            $btn_class = "btn-danger";
            break;
        case "canceled":
            $btn_class = "btn-secondary";
            break;
        default:
            $btn_class = "btn-warning";
            break;
    }
    $updated = $pipeline['updated_at'];             //Determine the last time the branch was updated
    $since = since($updated, 1);                    //Time since the last update
    $badge = "<span class=\"badge badge-secondary\">$since</span>"; //create a badge with the time since last update for on the button
    return "<a class=\"btn $btn_class\" href=\"$url\" role=\"button\" target=\"_$project\">$text $badge</a>";
}

function location($job) {
    // Determine the location of the job
    $location = "Unknown Location";             //standard response
    if (array_key_exists('runner', $job)) {     // if the runner key exists
        // if $job['runner'] and a description in jonb['runner'] exists, that is the location
        $location = ($job['runner'] != null) && array_key_exists('description', $job['runner']) ? $job['runner']['description'] : $location;

        if (startswith($location, 'runners-k8s-cvmfs-')) {
            $location = 'IT CVMFS';
        } elseif (startswith($location, 'runners-k8s-')) {
            $location = 'IT';
        }
    }
    return $location;
}

//EXECUTING
$data = array(); //ceate data array
foreach($projects as $index=>$project) {    //Loop over each index,project, each loop here is 1 column
    $parts = explode("/", $project, 2);     //Split the project on '/' into max 2 parts
    $used_token = null;
    if (count($parts) == 2) {               //If there are 2 parts
        $group = $parts[0];                 //Define group and Project
        $project = $parts[1];
        $used_token = $dev_token;
    } else {                                //If there is 1 part (there was no '/')
        $group = $default_group;            //The defined default group is the group
        $project = $parts[0];               //And only part is the project
        $used_token = $token;
    }

    // get the info
    $url = "https://gitlab.cern.ch/api/v4/projects/$group%2F$project/pipelines?per_page=$per_page"; //Define the url to use
    $pipelines = get($url, $used_token);                                                                 //Get the pipelines from the url

    $row = array();     //Create output variable
    $row[] = $index;    //add row number to the row

    $not_found = !$pipelines || array_key_exists('message', $pipelines);    //If pipelines is empty the key 'message' exists in the pipelines $not_found is true
    if ($not_found) {   //If the pipelines were not found
        // first column
        $row[] = label($group, $project) . " - NOT FOUND";  //make a label that says they are not found
        foreach($branches as $branch) { //create an empty branch column
            $row[] = "";
        }
        $row[] = "";    //create an empty other_branch column
    }

    else {            //If the pipelines were found
        // first column
        $row[] = label($group, $project);   //create a label with the project name

        // branch columns
        foreach($branches as $branch) {                                     //for each defined branch (4.2.x, master)
            if ($long_load) {
                $branch_pipeline = find_first($pipelines, $branch);             //Find these branches
                list($job,$failed_job) = find_job($group, $project, $branch_pipeline, $used_token);    //Load the jobs of the branches
                $location = location($job);                                     //Find out where they run

                if (array_key_exists('web_url', $failed_job)) {
                    $url = $failed_job['web_url'];
                }
                else if (array_key_exists('web_url', $job)) {
                    $url = $job['web_url'];
                }
                else {
                    $url = "https://gitlab.cern.ch/$group/$project/-/pipelines/";
                }

                $row[] = button($project, $branch_pipeline, $url, $location);   //Make a button for the branch in the column
            }
            else {
                $url = "https://gitlab.cern.ch/$group/$project";
                $branch_pipeline = find_first($pipelines, $branch);             //Find pipelines of these branches
                $row[] = button($project, $branch_pipeline, $url, $branch).' ';  //Add a button to the cell for each branch
            }
        }

        // other branches
        $other_branches = find_other_branches($pipelines, $branches, 5);    //find up to 5 branches
        $cell = "";                                                         //empty cell for the branches
        foreach($other_branches as $branch) {                               //for each other_branch
            $branch_pipeline = find_first($pipelines, $branch);             //Find pipelines of these branches
            $cell .= button($project, $branch_pipeline, $branch_pipeline['web_url'], $branch).' ';  //Add a button to the cell for each branch
        }
        $row[] = $cell;                                                     //Add the cell to the column
    }
    $data[] = $row;         //Add the row to the Data
}

// add initial key
$output['data'] = $data;    //Add initial key for json_encode
print(json_encode($output, JSON_PRETTY_PRINT));
?>

<?php

$default_group = 'atlas-otp';    //Default group if no group is defined
$branches = 'master';        // The  branches that you always want to test

// In order of dependency               //The projects that you want to test
$projects =
    'otp-requirements,otp-docs,otp-db-performance,otp-user-manual,atlasotp,otp-tools,otp-next,otp-next-cron,otp-reports,otp-www-next,otp-www-cron,otp-qualification,otp-appointment,otp-photos,otp-db-batch,otp-gitlab-ci-runner,otp-mail,otp-ui-default'
;

?>

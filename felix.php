<?php

$default_group = 'atlas-tdaq-felix';    //Default group if no group is defined
$branches = '4.2.x,master';      // The  branches that you always want to test

// In order of dependency               //The projects that you want to test
$projects =
    'felix-interface,cmake_tdaq,felix-client-thread,tdaq_tools,felix-mapper,hdlc_coder,python_env,wuppercodegen,regmap,flxcard,flxcard_py,felig-tools,felix-tag,felix-def,felix-unit-test,data_transfer_tools,felix-bus-fs,netio-next,netio3-backend,felix-client,felix-star,felix-starter,elinkconfig,ftools,felixpy,felixbus,felixbus-client,netio,packetformat,felixbase,felixcore,felix-distribution,felix-image,atlas-tdaq-felix-dev/clowder,atlas-tdaq-felix-dev/felix-user-manual,atlas-tdaq-felix-dev/felix-developer-manual,atlas-tdaq-felix-dev/felix-doc'
;

?>
